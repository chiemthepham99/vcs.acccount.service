from flask import jsonify
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, decode_token
from werkzeug.security import check_password_hash
from flask_pymongo import PyMongo
import pymongo
from datetime import date
from ultility import to_camel_case
import connexion
from flask_cors import CORS


def login(body):
    result = {
        "data": {
            'access_token': None
        }
    }
    # body = connexion.request.json

    filters = {
        'username': body.get('username')
    }
    user = mongo.db.users.find_one(filter=filters)
    http_code = 200
    if not user or not check_password_hash(user.get('password'), body.get('password')):
        result = {
            'errorMessage': "User or Password was wrong",
        }
        http_code = 400
    else:
        identity = {
            "username": user.get('username'),
            "is_active": user.get('is_active'),
            "roles": user.get('roles')
        }
        result['data']['access_token'] = create_access_token(identity=identity, expires_delta=False)

    return jsonify(result), http_code


def get_accounts(pageSize=10, page=1, orderBy=None, accountName=None, accountNumber=None, accountType=None,
                 customerGender=None, customerAge=None, accountStatus=None, fromDate=None, toDate=None):
    filters = {}
    if accountStatus is not None:
        filters['account_status'] = {'$eq': accountStatus}

    if accountType is not None:
        filters['account_type'] = {'$eq': accountType}

    if accountNumber is not None:
        filters['account_number'] = {'$regex': accountNumber, '$options': 'i'}

    if accountName is not None:
        filters['account_name'] = {'$regex': accountName, '$options': 'i'}

    if customerGender is not None:
        filters['customer_gender'] = {'$eq': customerGender}

    if customerAge is not None:
        today = date.today()
        year = today.year - int(customerAge)
        max_birthday = date.max.replace(year=year).strftime('%Y-%m-%d')
        min_birthday = date.min.replace(year=year).strftime('%Y-%m-%d')
        filters['customer_pd_dob'] = {'$lt': max_birthday, '$gt': min_birthday}

    if fromDate is not None and toDate is not None:
        filters['customer_pd_dob'] = {'$lt': toDate, '$gt': fromDate}

    offset = (page - 1) * pageSize

    if not pageSize or pageSize < 0:
        pageSize = 10

    sort = []
    if orderBy is not None:
        order_by = orderBy.split(",")
        for item in order_by:
            type_sort = pymongo.ASCENDING
            key_name = item
            if item.find("-") == 0:
                type_sort = pymongo.DESCENDING
                key_name = item[1:]
            sort.append((key_name, type_sort))

    items = mongo.db.accounts.find(filter=filters, projection={'_id': False}, limit=pageSize, skip=offset, sort=sort)
    total = items.count()
    data = []
    for item in items:
        account = {}
        for k in item:
            camel_k = to_camel_case(k)
            account[camel_k] = item[k]
        data.append(account)
    result = {
        'data': data,
        'pagination': {
            'current': page,
            'total': total,
            'pageSize': pageSize,
        }
    }
    return jsonify(result)


def get_account(account_number):
    filters = {'account_number': str(account_number)}
    account = mongo.db.accounts.find_one(filter=filters, projection={'_id': False})
    http_code = 200
    if account is None:
        result = {
            'errorMessage': "account not found",
        }
        http_code = 400
    else:
        converted_account = {}
        for k in account:
            camel_k = to_camel_case(k)
            converted_account[camel_k] = account[k]
        result = {
            'data': converted_account,
        }

    return jsonify(result), http_code


@jwt_required()
def create_account():
    account = connexion.request.json.get('account')
    current_user = get_jwt_identity()
    if current_user is not None:
        # @todo: check quyen
        account_number = account.get('account_number')
        filters = {'account_number': str(account_number)}
        exists_account = mongo.db.accounts.find_one(filter=filters)
        http_code = 200
        if exists_account is not None:
            http_code = 400
            result = {
                'errorMessage': "account exists!",
            }
        else:
            account_info = {
                'account_number': account.get('accountNumber'),
                'account_type': account.get('accountType'),
                'account_name': account.get('accountName'),
                'account_balance': account.get('accountBalance'),
                'account_status': account.get('accountStatus'),
                'customer_pd_dob': account.get('customerPdDob'),
                'customer_gender': account.get('customerGender'),
                'customer_email': account.get('customerEmail'),
                'customer_province': account.get('customerProvince'),
                'customer_district': account.get('customerDistrict'),
                'customer_address': account.get('customerAddress'),
                'created_at': account.get('createdAt'),
                'updated_at': account.get('updatedAt'),
                'create_user': account.get('createUser'),
                'update_user': account.get('updateUser')
            }
            _result = mongo.db.accounts.insert(account_info)
            new_account = mongo.db.accounts.find_one(
                filter={'_id': _result}
            )

            result = {
                'account': {
                    'accountNumber': new_account.get('account_number'),
                    'accountType': new_account.get('account_type'),
                    'accountName': new_account.get('account_name'),
                    'accountBalance': new_account.get('account_balance'),
                    'accountStatus': new_account.get('account_status'),
                    'customerPdDob': new_account.get('customer_pd_dob'),
                    'customerGender': new_account.get('customer_gender'),
                    'customerEmail': new_account.get('customer_email'),
                    'customerProvince': new_account.get('customer_province'),
                    'customerDistrict': new_account.get('customer_district'),
                    'customerAddress': new_account.get('customer_address'),
                    'createdAt': new_account.get('created_at'),
                    'updatedAt': new_account.get('updated_at'),
                    'createUser': new_account.get('create_user'),
                    'updateUser': new_account.get('update_user')
                }
            }
    else:
        result = {
            'errorMessage': "You must be logged in"
        }
        http_code = 401

    return jsonify(result), http_code


@jwt_required()
def update_account(account_number):
    current_user = get_jwt_identity()
    account = connexion.request.json.get('account')
    if current_user is not None:
        # @todo: check quyen
        filters = {'account_number': str(account_number)}
        fields = {'_id': False}
        exist_account = mongo.db.accounts.find_one(filter=filters, projection=fields)
        http_code = 200
        if exist_account is None:
            result = {
                'errorMessage': "account not found",
            }
            http_code = 400
        else:
            account_info = {
                'account_number': account.get('accountNumber'),
                'account_type': account.get('accountType'),
                'account_name': account.get('accountName'),
                'account_balance': account.get('accountBalance'),
                'account_status': account.get('accountStatus'),
                'customer_pd_dob': account.get('customerPdDob'),
                'customer_gender': account.get('customerGender'),
                'customer_email': account.get('customerEmail'),
                'customer_province': account.get('customerProvince'),
                'customer_district': account.get('customerDistrict'),
                'customer_address': account.get('customerAddress'),
                'created_at': account.get('createdAt'),
                'updated_at': account.get('updatedAt'),
                'create_user': account.get('createUser'),
                'update_user': account.get('updateUser')
            }
            mongo.db.accounts.update(filters, {'$set': account_info})
            result = {
                'account': account_info,
            }
    else:
        result = {
            'errorMessage': "You must be logged in"
        }
        http_code = 401
    return jsonify(result), http_code


@jwt_required()
def delete_account(account_number):
    current_user = get_jwt_identity()
    http_code = 200
    if current_user is not None:
        # @todo: check quyen
        filters = {'account_number': str(account_number)}
        projection = {'_id': False}
        exist_account = mongo.db.accounts.find_one(filter=filters, projection=projection)

        if exist_account is None:
            result = {
                'errorMessage': "Account not found"
            }
            http_code = 400
        else:
            mongo.db.accounts.remove(filters)
            result = {
                'ok': True
            }
    else:
        result = {
            'errorMessage': "You must be logged in"
        }
        http_code = 401
    return jsonify(result), http_code


def app_decode_token(token):
    return decode_token(token)


def create_app():
    cx = connexion.App(__name__)
    cx.add_api('swagger.yaml')
    flask_app = cx.app
    return flask_app


app = create_app()

CORS(app, resources={r"/*": {"origins": "*"}})
app.config["MONGO_URI"] = "mongodb://202.182.111.45:27017/bank2"
app.config["JWT_SECRET_KEY"] = "a123B456cC@!#123"
app.config["PORT"] = "8081"
app.config["HOST"] = "0.0.0.0"
mongo = PyMongo(app)
jwt = JWTManager()
jwt.init_app(app)

if __name__ == '__main__':
    app.run()